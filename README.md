[[_TOC_]]

# [scli](https://github.com/isamert/scli/) Packages

## Installation

### signal-cli

* [Documentation](https://packaging.gitlab.io/signal-cli)

### scli

#### Install prerequisites

```bash
sudo apt-get install curl apt-transport-https lsb-release
```

#### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-scli.asc https://packaging.gitlab.io/scli/gpg.key
```

#### Add repo to apt

```bash
echo "deb https://packaging.gitlab.io/scli/repo-$(lsb_release -sc) $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/morph027-scli.list
```

### Install packages

```bash
sudo apt-get install python3-scli
```

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
