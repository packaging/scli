#!/bin/bash

set -e

if [ -n "$DEBUG" ]; then
  set -x
fi

. /etc/lsb-release

export DEBIAN_FRONTEND=noninteractive
tag="${CI_COMMIT_TAG:-v0.0.0-0}"
version="${tag%%-*}"
iteration="${tag##*-}"

printf "\e[0;36mInstalling toolchain ...\e[0m\n"
case "${DISTRIB_CODENAME}" in
  "bionic")
    python="python3.8"
    ;;
  *)
    python="python3"
    ;;
esac
apt-get -qq update
apt-get install -qqy \
  software-properties-common
add-apt-repository ppa:git-core/ppa -y
apt-get install -qqy \
  "${python}" \
  "${python}"-dev \
  python3-pip \
  curl \
  git \
  build-essential \
  ruby \
  ruby-dev \
  libffi-dev
type fpm &>/dev/null || gem install fpm --no-document
lib_path="$("${python}" -c 'from distutils.sysconfig import get_python_lib; print(get_python_lib())')"

fpm_args=""

case "${NAME}" in
  "scli")
    if [ ! -d /tmp/scli ]; then
      git clone -b "${version}" https://github.com/isamert/scli/ /tmp/scli
    fi
    git -C /tmp/scli checkout "${version}"
    git -C /tmp/scli archive HEAD | tar -C /tmp/scli -xf - VERSION
    source="scli VERSION README.md"
    fpm_args+=" -s dir"
    fpm_args+=" --name python3-scli"
    fpm_args+=" --version ${version//v/}"
    fpm_args+=" --iteration ${iteration}"
    fpm_args+=" --depends python3-urwid"
    fpm_args+=" --depends python3-urwid-readline"
    fpm_args+=" --depends python3-pyqrcode"
    fpm_args+=" --depends signal-cli-jre"
    fpm_args+=" --depends dbus"
    fpm_args+=" -C /tmp/scli"
    fpm_args+=" --prefix /usr/share/scli"
    fpm_args+=" --architecture=all"
    fpm_args+=" --url https://github.com/isamert/scli/"
    fpm_args+=" --after-install ${CI_PROJECT_DIR}/.packaging/after-install.sh"
    fpm_args+=" --maintainer 'Stefan Heitmüller <stefan.heitmueller@gmx.com>'"
    case "${DISTRIB_CODENAME}" in
      "bionic")
        fpm_args+=" --depends python3.8"
        sed -i 's,^#!/usr/bin/env python3,#!/usr/bin/env python3.8,' /tmp/scli/scli
        ;;
      *)
        fpm_args+=" --depends python3"
        ;;
    esac
    ;;
  *)
    source="${NAME}"
    fpm_args+=" -s python"
    fpm_args+=" --iteration ${iteration}"
    fpm_args+=" --python-package-name-prefix python3"
    fpm_args+=" --python-pip /usr/bin/pip3"
    fpm_args+=" --python-bin /usr/bin/${python}"
    fpm_args+=" --python-install-bin ${PYTHON_INSTALL_BIN:-/usr/local/bin/python3}"
    fpm_args+=" --python-install-lib ${lib_path}"
    fpm_args+=" --architecture=all"
    ;;
esac

case "${NAME}" in
  "urwid-readline")
    fpm_args+=" --depends python3-urwid"
    ;;
esac

printf "\e[0;36mCreating \e[0;33m%s\e[0;36mdebian package ${NAME} ...\e[0m\n" "$DEP"
mkdir -p "${CI_PROJECT_DIR}/build-${DIST}"
cd "${CI_PROJECT_DIR}/build-${DIST}"
echo "fpm -f -t deb --deb-no-default-config-files ${fpm_args} ${source}" > /tmp/scli.fpm.sh
sh /tmp/scli.fpm.sh
