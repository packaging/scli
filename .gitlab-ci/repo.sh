#!/bin/bash

echo "$SIGNING_KEY" > /tmp/key
gpg --import /tmp/key
rm -f /tmp/key
test -f "${CI_PROJECT_DIR}"/.repo/gpg.key || gpg --export --armor "${SIGNING_KEY_ID}" > "${CI_PROJECT_DIR}"/.repo/gpg.key
sed -i 's,##SIGNING_KEY_ID##,'"${SIGNING_KEY_ID}"',g' ".repo/${DIST}/conf/distributions"
mkdir "${CI_PROJECT_DIR}/.repo/repo-${DIST}"
find "${CI_PROJECT_DIR}/build-${DIST}" -type f -name "*.deb" -exec \
  reprepro \
  -v \
  --outdir "${CI_PROJECT_DIR}/.repo/repo-${DIST}" \
  -b "${CI_PROJECT_DIR}/.repo/${DIST}" \
  includedeb \
  "${DIST}" \
  {} \+;
